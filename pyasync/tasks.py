import datetime
import json
import logging
import math
import uuid
from abc import ABCMeta, abstractmethod
from enum import Enum
from typing import NamedTuple, Dict, Any, Optional
import threading
from typing import Type
import time

import boto3
from mypy_boto3_dynamodb.client import DynamoDBClient
from mypy_boto3_sqs.client import SQSClient
import pytz
from six import with_metaclass

from . import utils, error_codes, settings

logger = logging.getLogger(__name__)


class AsyncTaskState(Enum):
    INITIAL = 1
    QUEUED = 2
    STARTED = 3
    DONE = 4


class AsyncTask(NamedTuple):
    name: str
    kwargs: Dict[str, Any]
    ttl: int = -1


class AsyncTaskRecord(NamedTuple):
    task_guid: uuid.UUID
    task_name: str
    task_kwargs: Dict[str, Any]
    created: datetime.datetime
    updated: datetime.datetime
    expires: datetime.datetime
    duration: float
    state: AsyncTaskState
    result: Optional[Dict[str, Any]] = None
    error: Optional[bool] = False


class AsyncTaskError(Exception):
    pass


class AsyncTaskAlreadyProcessed(AsyncTaskError):
    pass


def _now():
    return datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)


def _to_unix_timestamp(dt: datetime.datetime):
    return math.ceil(dt.timestamp())


class AbstractAsyncTaskEngine(with_metaclass(ABCMeta, object)):
    def __init__(self):
        self.functions = {}

    def register_function(self, name, fnc):
        self.functions[name] = fnc

    @classmethod
    def create(cls) -> 'AbstractAsyncTaskEngine':
        pass

    @classmethod
    def generate_task_guid(cls) -> uuid.UUID:
        return uuid.uuid4()

    @abstractmethod
    def on_create_task_record(
            self, task_guid: uuid.UUID, task_name: str, task_kwargs: Dict[str, Any], ttl: int = -1) -> bool:
        pass

    @abstractmethod
    def on_update_task_record(
            self, task_guid: uuid.UUID, state: AsyncTaskState = None, result: Dict[str, Any] = None,
            error: bool = False, elapsed_time: float=-1) -> bool:
        pass

    @abstractmethod
    def on_notify_task_created(
            self, task_guid: uuid.UUID) -> bool:
        pass

    @abstractmethod
    def on_retrieve_task_record(self, task_guid: uuid.UUID) -> AsyncTaskRecord:
        pass

    def schedule(self, task_name: str, task_kwargs: Dict[str, Any], ttl: int = -1) -> AsyncTaskRecord:
        # Check if task name is registered task
        if task_name not in self.functions:
            raise AsyncTaskError(f'Unknown function: {task_name}')

        # Create new task guid
        task_guid = uuid.uuid4()

        if not self.on_create_task_record(task_guid=task_guid, task_name=task_name, task_kwargs=task_kwargs, ttl=ttl):
            raise AsyncTaskError('Could not create task record')

        if not self.on_notify_task_created(task_guid=task_guid):
            raise AsyncTaskError('Could not notify task created')

        if not self.on_update_task_record(task_guid=task_guid,  state=AsyncTaskState.QUEUED):
            raise AsyncTaskError('Could not update task record')

        return self.on_retrieve_task_record(task_guid=task_guid)

    def run(self, task_guid: uuid.UUID) -> bool:
        # Retrieve task record
        task_record = self.on_retrieve_task_record(task_guid=task_guid)

        # Make sure the task is not yet running
        if task_record.state == AsyncTaskState.STARTED:
            raise AsyncTaskAlreadyProcessed('The task has already started')
        elif task_record.state == AsyncTaskState.DONE:
            raise AsyncTaskAlreadyProcessed('The task has already finished')

        # Update record
        if not self.on_update_task_record(task_guid=task_guid,  state=AsyncTaskState.STARTED):
            raise AsyncTaskError('Could not update task record')

        # Call function and store result
        error = False
        t = time.process_time()
        try:
            fnc = self.functions[task_record.task_name]
            result = fnc(**task_record.task_kwargs)
            elapsed_time = time.process_time() - t
        except Exception as e:
            error = True
            elapsed_time = time.process_time() - t
            result = AsyncTaskResultFactory(
                error=True, error_code=error_codes.UNHANDLED_ERROR, exception=e, reason=str(e)).build()

        # Update record
        if not self.on_update_task_record(
                task_guid=task_guid, state=AsyncTaskState.DONE, error=error, result=result, elapsed_time=elapsed_time):
            raise AsyncTaskError('Could not update task record')

        # Return value indicates success
        return error is False

    def get(self, task_guid: uuid.UUID):
        return self.on_retrieve_task_record(task_guid=task_guid)


class LocalThreadTaskEngine(AbstractAsyncTaskEngine):
    def __init__(self):
        super().__init__()
        self.results_table = {}

    @classmethod
    def create(cls):
        return cls()

    def on_create_task_record(self, task_guid: uuid.UUID, task_name: str, task_kwargs: Dict[str, Any],
                              ttl: int = -1) -> bool:
        logger.debug(f'on_create_task_record: {task_guid}: {task_name}({task_kwargs})')

        # Define created, updated and expires datetimes
        created = _now()
        updated = _now()
        expires = created + datetime.timedelta(seconds=ttl if ttl > 0 else settings.ASYNC_TASK_DEFAULT_TTL)

        self.results_table[task_guid] = {
            'created': created,
            'updated': updated,
            'duration': -1,
            'time_to_live': expires,
            'state': AsyncTaskState.INITIAL.name,
            'error': False,
            'name': task_name,
            'kwargs': task_kwargs,
            'result': {}
        }

        return True

    def on_update_task_record(self, task_guid: uuid.UUID, state: AsyncTaskState = None, result: Dict[str, Any] = None,
                              error: bool = False, elapsed_time: float=-1) -> bool:
        logger.debug(f'on_update_task_record: {task_guid}: state: {state}, result: {result}, error: {error}, '
                     f'elapsed time: {elapsed_time}')

        # Define updated timestamp
        updated = _now()

        update = {
            'updated': updated,
        }

        # Add state update
        if state is not None:
            update['state'] = state.name

        # Add duration update
        if state == state.DONE and elapsed_time > 0:
            update['duration'] = elapsed_time

        # Add result update
        if result is not None:
            update['result'] = result

        # Add error update
        if error:
            update['error'] = error

        self.results_table[task_guid].update(update)

        return True

    def on_notify_task_created(self, task_guid: uuid.UUID) -> bool:
        logger.debug(f'on_notify_task_created: {task_guid}')

        task_name = self.get(task_guid).task_name
        task_thread = threading.Thread(
            target=lambda : self.run(task_guid),
            name=f'task-{task_guid}:{task_name}')
        task_thread.start()

        return True

    def on_retrieve_task_record(self, task_guid: uuid.UUID) -> Optional[AsyncTaskRecord]:
        if task_guid not in self.results_table:
            return

        item = self.results_table[task_guid]
        return AsyncTaskRecord(
            task_guid=task_guid,
            task_name=item['name'],
            task_kwargs=item['kwargs'],
            created=item['created'],
            expires=item['time_to_live'],
            updated=item['updated'],
            state=AsyncTaskState[item['state']],
            result=item['result'],
            error=item['error'],
            duration=item['duration'])


class AWSAsyncTaskEngine(AbstractAsyncTaskEngine):
    def __init__(self, dynamodb_table: str, sqs_url: str):
        super().__init__()

        self.dynamodb_table = dynamodb_table
        self.sqs_url = sqs_url

        self.dynamodb_client: DynamoDBClient = None
        self.sqs_client: SQSClient = None

    @classmethod
    def create(cls):
        return AWSAsyncTaskEngine(
            dynamodb_table=settings.PYASYNC_AWS_ENGINE_RESPONSE_TABLE,
            sqs_url=settings.PYASYNC_AWS_ENGINE_REQUESTS_QUEUE)

    def on_create_task_record(
            self, task_guid: uuid.UUID, task_name: str, task_kwargs: Dict[str, Any], ttl: int = -1) -> bool:

        logger.debug(f'on_create_task_record: {task_guid}: {task_name}({task_kwargs})')

        # Define created, updated and expires datetimes
        created = _now()
        updated = _now()
        expires = created + datetime.timedelta(seconds=ttl if ttl > 0 else settings.PYASYNC_DEFAULT_TASK_TTL)

        # Convert to unix timestamps
        created_timestamp = _to_unix_timestamp(updated)
        updated_timestamp = _to_unix_timestamp(updated)
        expires_timestamp = _to_unix_timestamp(expires)

        response = self.get_dynamodb_client().put_item(
            TableName=self.dynamodb_table,
            Item={
                'Guid': {'S': str(task_guid)},
                'Created': {'N': str(created_timestamp)},
                'Updated': {'N': str(updated_timestamp)},
                'Duration': {'N': '-1'},
                'TimeToLive': {'N': str(expires_timestamp)},
                'State': {'S': AsyncTaskState.INITIAL.name},
                'Error': {'BOOL': False},
                'Name': {'S': task_name},
                'Kwargs': {'S': json.dumps(task_kwargs)},
                'Result': {'S': json.dumps({})},
            }
        )

        logger.debug(json.dumps(response))

        return response['ResponseMetadata']['HTTPStatusCode'] == 200

    def on_update_task_record(
            self, task_guid: uuid.UUID, state: AsyncTaskState = None, result: Dict[str, Any] = None,
            error: bool = False, elapsed_time: float=-1) -> bool:
        logger.debug(f'on_update_task_record: {task_guid}: state: {state}, result: {result}, error: {error}, '
                     f'elapsed time: {elapsed_time}')

        # Define updated timestamp
        updated_timestamp = _to_unix_timestamp(_now())

        # Define default attribute names
        attribute_names = {
            '#U': 'Updated',
        }
        # Define default attribute values
        attribute_values = {
            ':u': {'N': str(updated_timestamp)}
        }

        # Define default update expression
        update_expression = 'SET #U = :u'

        # Add state update
        if state is not None:
            attribute_names['#S'] = 'State'
            attribute_values[':s'] = {'S': state.name}
            update_expression += ', #S = :s'

        # Add duration update
        if state == state.DONE and elapsed_time > 0:
            attribute_names['#D'] = 'Duration'
            attribute_values[':d'] = {'N': str(elapsed_time)}
            update_expression += ', #D = :d'

        # Add result update
        if result is not None:
            attribute_names['#R'] = 'Result'
            attribute_values[':r'] = {'S': json.dumps(result)}
            update_expression += ', #R = :r'

        # Add error update
        if error:
            attribute_names['#E'] = 'Error'
            attribute_values[':e'] = {'BOOL': error}
            update_expression += ', #E = :e'

        response = self.get_dynamodb_client().update_item(
            TableName=self.dynamodb_table,
            Key={
                'Guid': {'S': str(task_guid)},
            },
            ExpressionAttributeNames=attribute_names,
            ExpressionAttributeValues=attribute_values,
            UpdateExpression=update_expression,
        )

        logger.debug(json.dumps(response))

        return response['ResponseMetadata']['HTTPStatusCode'] == 200

    def on_retrieve_task_record(self, task_guid: uuid.UUID) -> Optional[AsyncTaskRecord]:
        response = self.get_dynamodb_client().get_item(
            TableName=self.dynamodb_table,
            Key={
                'Guid': {
                    'S': str(task_guid)
                }
            })

        if not 'Item' in response:
            return None

        item = response['Item']
        return AsyncTaskRecord(
            task_guid=uuid.UUID(item['Guid']['S']),
            task_name=item['Name']['S'],
            task_kwargs=json.loads(item['Kwargs']['S']),
            created=datetime.datetime.fromtimestamp(int(item['Created']['N'])).replace(tzinfo=pytz.UTC),
            expires=datetime.datetime.fromtimestamp(int(item['TimeToLive']['N'])).replace(tzinfo=pytz.UTC),
            updated=datetime.datetime.fromtimestamp(int(item['Updated']['N'])).replace(tzinfo=pytz.UTC),
            duration=float(item['Duration']['N']),
            state=AsyncTaskState[item['State']['S']],
            result=json.loads(item['Result']['S']),
            error=item['Error']['BOOL'])

    def on_notify_task_created(
            self, task_guid: uuid.UUID) -> bool:

        logger.debug(f'on_notify_task_created: {task_guid} (queue: {self.sqs_url})')

        response = self.get_sqs_client().send_message(
            QueueUrl=self.sqs_url,
            MessageBody=json.dumps({
                'Guid': str(task_guid),
            })
        )

        # response = self.get_sqs_client().publish(
        #     TargetArn=self.sns_topic_arn,
        #     Message=json.dumps({'default': json.dumps({
        #         'Guid': str(task_guid),
        #     })}),
        #     MessageStructure='json'
        # )

        logger.debug(json.dumps(response))

        return response['ResponseMetadata']['HTTPStatusCode'] == 200

    def get_dynamodb_client(self) -> DynamoDBClient:
        if self.dynamodb_client is None:
            self.dynamodb_client = utils.create_dynamodb_client(
                session=boto3.Session(profile_name=settings.AWS_PROFILE))
        return self.dynamodb_client

    def get_sqs_client(self) -> SQSClient:
        if self.sqs_client is None:
            self.sqs_client = utils.create_sqs_client(session=boto3.Session(profile_name=settings.AWS_PROFILE))
        return self.sqs_client


class AsyncTaskResultFactory(object):
    def __init__(self, result: Optional[Dict[str, Any]]=None, error: bool=False, error_code: int=error_codes.NO_ERROR,
                 exception: BaseException=None, reason: str=None):
        self.result = result
        self.error = error
        self.error_code = error_code
        self.exception = exception
        self.reason = reason

    def build(self):
        return {
            'result': self.result,
            'error': self.error,
            'error_code': self.error_code,
            'reason': self.reason
        }


_DEFAULT_TASK_ENGINE = None  # type: Optional[AbstractAsyncTaskEngine]


def create_task_engine(task_engine_class_name: str) -> AbstractAsyncTaskEngine:
    task_engine_class = utils.import_string(task_engine_class_name)  # type: Type[AbstractAsyncTaskEngine]
    return task_engine_class.create()


def get_default_task_engine() -> AbstractAsyncTaskEngine:
    global _DEFAULT_TASK_ENGINE
    if _DEFAULT_TASK_ENGINE is None:
        _DEFAULT_TASK_ENGINE = create_task_engine(settings.PYASYNC_DEFAULT_TASK_ENGINE)
    return _DEFAULT_TASK_ENGINE


def register_async_task(task_engine: AbstractAsyncTaskEngine, fnc: callable):
    task_engine.register_function(fnc.__name__, fnc)
    return fnc
