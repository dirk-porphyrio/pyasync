#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def get_version(*file_paths):
    """Retrieves the version from pyasync/__init__.py"""
    filename = os.path.join(os.path.dirname(__file__), *file_paths)
    version_file = open(filename).read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


version = get_version("pyasync", "__init__.py")


# if sys.argv[-1] == 'publish':
#     try:
#         import wheel
#         print("Wheel version: ", wheel.__version__)
#     except ImportError:
#         print('Wheel library missing. Please run "pip install wheel"')
#         sys.exit()
#     os.system('python setup.py sdist upload')
#     os.system('python setup.py bdist_wheel upload')
#     sys.exit()
#
# if sys.argv[-1] == 'tag':
#     print("Tagging the version on git:")
#     os.system("git tag -a %s -m 'version %s'" % (version, version))
#     os.system("git push --tags")
#     sys.exit()


setup(
    name='pyasync',
    version=version,
    description="""A library providing an async task mechanism""",
    author='Dirk Moors',
    author_email='dirk.moors@porphyrio.com',
    url='https://bitbucket.org/dirk-porphyrio/pyasync',
    packages=[
        'pyasync',
    ],
    include_package_data=True,
    install_requires=[
        # 'Django>=1.8,<=1.12.0',
        # 'djangorestframework>=3.5.4',
        # 'jsonfield>=2.0.0',
        # 'django-fsm==2.6.0',
        # 'six>=1.11.0',
        'boto3~=1.15.8',
        'boto3-stubs[s3,lambda,dynamodb,sqs]~=1.15.8.0',
        'pytz~=2020.1',
        'six~=1.15.0'
    ],
    license="MIT",
    zip_safe=False,
    keywords='pyasync',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
)
