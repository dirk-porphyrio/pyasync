import glob
import logging
import os
from importlib import import_module

import boto3
from mypy_boto3_s3.client import S3Client
from mypy_boto3_dynamodb.client import DynamoDBClient
from mypy_boto3_sqs.client import SQSClient

logger = logging.getLogger(__name__)


def import_string(dotted_path):
    """
    Import a dotted module path and return the attribute/class designated by the
    last name in the path. Raise ImportError if the import failed.
    """
    try:
        module_path, class_name = dotted_path.rsplit('.', 1)
    except ValueError as err:
        raise ImportError("%s doesn't look like a module path" % dotted_path) from err

    module = import_module(module_path)

    try:
        return getattr(module, class_name)
    except AttributeError as err:
        raise ImportError('Module "%s" does not define a "%s" attribute/class' % (
            module_path, class_name)
        ) from err


def create_s3_client(session: boto3.session.Session=None) -> S3Client:
    session = session or boto3.session.Session()

    sts_client = session.client('sts')
    identity = sts_client.get_caller_identity()
    logger.debug(f'create_s3_client: identity: {identity}')

    return session.client('s3')


def create_dynamodb_client(session: boto3.session.Session=None) -> DynamoDBClient:
    session = session or boto3.session.Session()

    sts_client = session.client('sts')
    identity = sts_client.get_caller_identity()
    logger.debug(f'create_dynamodb_client: identity: {identity}')

    return session.client('dynamodb')


def create_sqs_client(session: boto3.session.Session=None) -> SQSClient:
    session = session or boto3.session.Session()

    sts_client = session.client('sts')
    identity = sts_client.get_caller_identity()
    logger.debug(f'create_sqs_client: identity: {identity}')

    return session.client('sqs')
