import json
import uuid
import logging

from . import settings, tasks

####################################################################
# Logger
####################################################################
rootLogger = logging.getLogger('')
rootLogger.setLevel(logging.INFO)

logger = logging.getLogger(logging.getLevelName(settings.LOG_LEVEL))


####################################################################
# Functions
####################################################################
def process_async_task(event, context):
    def execute_task(task_guid: uuid.UUID):
        logger.debug(f'process_async_task: executing task {task_guid}')
        try:
            tasks.get_default_task_engine().run(task_guid=task_guid)
        except tasks.AsyncTaskAlreadyProcessed as e:
            logger.exception(f'process_async_task: task {task_guid} was already processed, skipping.', exc_info=e)

    for record in event['Records']:
        try:
            logger.debug(f'process_async_task: {record}')

            # Load json body
            msg = json.loads(record['body'])

            # Retrieve task guid
            task_guid = uuid.UUID(msg['Guid'])

            # Execute task
            execute_task(task_guid=task_guid)
        except Exception as e:
            logger.exception(f'process_async_task: failed to handle record: {record}', exc_info=e)


